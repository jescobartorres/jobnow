package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.JobnowApp;

import com.mycompany.myapp.domain.Oferta;
import com.mycompany.myapp.repository.OfertaRepository;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.mycompany.myapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the OfertaResource REST controller.
 *
 * @see OfertaResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = JobnowApp.class)
public class OfertaResourceIntTest {

    private static final String DEFAULT_TITULO = "AAAAAAAAAA";
    private static final String UPDATED_TITULO = "BBBBBBBBBB";

    private static final String DEFAULT_DETALLE = "AAAAAAAAAA";
    private static final String UPDATED_DETALLE = "BBBBBBBBBB";

    private static final String DEFAULT_LOCALIDAD = "AAAAAAAAAA";
    private static final String UPDATED_LOCALIDAD = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_FECHA = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_FECHA = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_TIPO_CONTRATO = "AAAAAAAAAA";
    private static final String UPDATED_TIPO_CONTRATO = "BBBBBBBBBB";

    private static final String DEFAULT_JORNADA = "AAAAAAAAAA";
    private static final String UPDATED_JORNADA = "BBBBBBBBBB";

    private static final Integer DEFAULT_SALARIO = 1;
    private static final Integer UPDATED_SALARIO = 2;

    @Autowired
    private OfertaRepository ofertaRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restOfertaMockMvc;

    private Oferta oferta;

    @Before
    public void setup() {
/*        final OfertaResource ofertaResource = new OfertaResource(ofertaRepository);
=======
        /*MockitoAnnotations.initMocks(this);
        final OfertaResource ofertaResource = new OfertaResource(ofertaRepository);
>>>>>>> 55c60cb8e116c9dcfd620c7039994685bbe7abc5
        this.restOfertaMockMvc = MockMvcBuilders.standaloneSetup(ofertaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
<<<<<<< HEAD
            .setMessageConverters(jacksonMessageConverter).build();
            */

    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Oferta createEntity(EntityManager em) {
        Oferta oferta = new Oferta()
            .titulo(DEFAULT_TITULO)
            .detalle(DEFAULT_DETALLE)
            .localidad(DEFAULT_LOCALIDAD)
            .fecha(DEFAULT_FECHA)
            .tipo_contrato(DEFAULT_TIPO_CONTRATO)
            .jornada(DEFAULT_JORNADA)
            .salario(DEFAULT_SALARIO);
        return oferta;
    }

    @Before
    public void initTest() {
        oferta = createEntity(em);
    }

    @Test
    @Transactional
    public void createOferta() throws Exception {
        int databaseSizeBeforeCreate = ofertaRepository.findAll().size();

        // Create the Oferta
        restOfertaMockMvc.perform(post("/api/ofertas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(oferta)))
            .andExpect(status().isCreated());

        // Validate the Oferta in the database
        List<Oferta> ofertaList = ofertaRepository.findAll();
        assertThat(ofertaList).hasSize(databaseSizeBeforeCreate + 1);
        Oferta testOferta = ofertaList.get(ofertaList.size() - 1);
        assertThat(testOferta.getTitulo()).isEqualTo(DEFAULT_TITULO);
        assertThat(testOferta.getDetalle()).isEqualTo(DEFAULT_DETALLE);
        assertThat(testOferta.getLocalidad()).isEqualTo(DEFAULT_LOCALIDAD);
        assertThat(testOferta.getFecha()).isEqualTo(DEFAULT_FECHA);
        assertThat(testOferta.getTipo_contrato()).isEqualTo(DEFAULT_TIPO_CONTRATO);
        assertThat(testOferta.getJornada()).isEqualTo(DEFAULT_JORNADA);
        assertThat(testOferta.getSalario()).isEqualTo(DEFAULT_SALARIO);
    }

    @Test
    @Transactional
    public void createOfertaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = ofertaRepository.findAll().size();

        // Create the Oferta with an existing ID
        oferta.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOfertaMockMvc.perform(post("/api/ofertas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(oferta)))
            .andExpect(status().isBadRequest());

        // Validate the Oferta in the database
        List<Oferta> ofertaList = ofertaRepository.findAll();
        assertThat(ofertaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllOfertas() throws Exception {
        // Initialize the database
        ofertaRepository.saveAndFlush(oferta);

        // Get all the ofertaList
        restOfertaMockMvc.perform(get("/api/ofertas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(oferta.getId().intValue())))
            .andExpect(jsonPath("$.[*].titulo").value(hasItem(DEFAULT_TITULO.toString())))
            .andExpect(jsonPath("$.[*].detalle").value(hasItem(DEFAULT_DETALLE.toString())))
            .andExpect(jsonPath("$.[*].localidad").value(hasItem(DEFAULT_LOCALIDAD.toString())))
            .andExpect(jsonPath("$.[*].fecha").value(hasItem(DEFAULT_FECHA.toString())))
            .andExpect(jsonPath("$.[*].tipo_contrato").value(hasItem(DEFAULT_TIPO_CONTRATO.toString())))
            .andExpect(jsonPath("$.[*].jornada").value(hasItem(DEFAULT_JORNADA.toString())))
            .andExpect(jsonPath("$.[*].salario").value(hasItem(DEFAULT_SALARIO)));
    }

    @Test
    @Transactional
    public void getOferta() throws Exception {
        // Initialize the database
        ofertaRepository.saveAndFlush(oferta);

        // Get the oferta
        restOfertaMockMvc.perform(get("/api/ofertas/{id}", oferta.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(oferta.getId().intValue()))
            .andExpect(jsonPath("$.titulo").value(DEFAULT_TITULO.toString()))
            .andExpect(jsonPath("$.detalle").value(DEFAULT_DETALLE.toString()))
            .andExpect(jsonPath("$.localidad").value(DEFAULT_LOCALIDAD.toString()))
            .andExpect(jsonPath("$.fecha").value(DEFAULT_FECHA.toString()))
            .andExpect(jsonPath("$.tipo_contrato").value(DEFAULT_TIPO_CONTRATO.toString()))
            .andExpect(jsonPath("$.jornada").value(DEFAULT_JORNADA.toString()))
            .andExpect(jsonPath("$.salario").value(DEFAULT_SALARIO));
    }

    @Test
    @Transactional
    public void getNonExistingOferta() throws Exception {
        // Get the oferta
        restOfertaMockMvc.perform(get("/api/ofertas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOferta() throws Exception {
        // Initialize the database
        ofertaRepository.saveAndFlush(oferta);
        int databaseSizeBeforeUpdate = ofertaRepository.findAll().size();

        // Update the oferta
        Oferta updatedOferta = ofertaRepository.findOne(oferta.getId());
        // Disconnect from session so that the updates on updatedOferta are not directly saved in db
        em.detach(updatedOferta);
        updatedOferta
            .titulo(UPDATED_TITULO)
            .detalle(UPDATED_DETALLE)
            .localidad(UPDATED_LOCALIDAD)
            .fecha(UPDATED_FECHA)
            .tipo_contrato(UPDATED_TIPO_CONTRATO)
            .jornada(UPDATED_JORNADA)
            .salario(UPDATED_SALARIO);

        restOfertaMockMvc.perform(put("/api/ofertas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedOferta)))
            .andExpect(status().isOk());

        // Validate the Oferta in the database
        List<Oferta> ofertaList = ofertaRepository.findAll();
        assertThat(ofertaList).hasSize(databaseSizeBeforeUpdate);
        Oferta testOferta = ofertaList.get(ofertaList.size() - 1);
        assertThat(testOferta.getTitulo()).isEqualTo(UPDATED_TITULO);
        assertThat(testOferta.getDetalle()).isEqualTo(UPDATED_DETALLE);
        assertThat(testOferta.getLocalidad()).isEqualTo(UPDATED_LOCALIDAD);
        assertThat(testOferta.getFecha()).isEqualTo(UPDATED_FECHA);
        assertThat(testOferta.getTipo_contrato()).isEqualTo(UPDATED_TIPO_CONTRATO);
        assertThat(testOferta.getJornada()).isEqualTo(UPDATED_JORNADA);
        assertThat(testOferta.getSalario()).isEqualTo(UPDATED_SALARIO);
    }

    @Test
    @Transactional
    public void updateNonExistingOferta() throws Exception {
        int databaseSizeBeforeUpdate = ofertaRepository.findAll().size();

        // Create the Oferta

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restOfertaMockMvc.perform(put("/api/ofertas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(oferta)))
            .andExpect(status().isCreated());

        // Validate the Oferta in the database
        List<Oferta> ofertaList = ofertaRepository.findAll();
        assertThat(ofertaList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteOferta() throws Exception {
        // Initialize the database
        ofertaRepository.saveAndFlush(oferta);
        int databaseSizeBeforeDelete = ofertaRepository.findAll().size();

        // Get the oferta
        restOfertaMockMvc.perform(delete("/api/ofertas/{id}", oferta.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Oferta> ofertaList = ofertaRepository.findAll();
        assertThat(ofertaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Oferta.class);
        Oferta oferta1 = new Oferta();
        oferta1.setId(1L);
        Oferta oferta2 = new Oferta();
        oferta2.setId(oferta1.getId());
        assertThat(oferta1).isEqualTo(oferta2);
        oferta2.setId(2L);
        assertThat(oferta1).isNotEqualTo(oferta2);
        oferta1.setId(null);
        assertThat(oferta1).isNotEqualTo(oferta2);
    }
}
