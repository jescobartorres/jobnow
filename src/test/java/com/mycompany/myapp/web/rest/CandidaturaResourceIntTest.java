package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.JobnowApp;

import com.mycompany.myapp.domain.Candidatura;
import com.mycompany.myapp.repository.CandidaturaRepository;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.EntityManager;
import java.util.List;

import static com.mycompany.myapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CandidaturaResource REST controller.
 *
 * @see CandidaturaResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = JobnowApp.class)
public class CandidaturaResourceIntTest {

    private static final String DEFAULT_CV_TEXTO = "AAAAAAAAAA";
    private static final String UPDATED_CV_TEXTO = "BBBBBBBBBB";

    private static final byte[] DEFAULT_CV_FICHERO = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_CV_FICHERO = TestUtil.createByteArray(2, "1");
    private static final String DEFAULT_CV_FICHERO_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_CV_FICHERO_CONTENT_TYPE = "image/png";

    private static final Boolean DEFAULT_APTO = false;
    private static final Boolean UPDATED_APTO = true;

    @Autowired
    private CandidaturaRepository candidaturaRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restCandidaturaMockMvc;

    private Candidatura candidatura;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

//        final CandidaturaResource candidaturaResource = new CandidaturaResource(candidaturaRepository);
//        this.restCandidaturaMockMvc = MockMvcBuilders.standaloneSetup(candidaturaResource)
//            .setCustomArgumentResolvers(pageableArgumentResolver)
//            .setControllerAdvice(exceptionTranslator)
//            .setConversionService(createFormattingConversionService())
//            .setMessageConverters(jacksonMessageConverter).build();

    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Candidatura createEntity(EntityManager em) {
        Candidatura candidatura = new Candidatura()
            .cv_texto(DEFAULT_CV_TEXTO)
            .cv_fichero(DEFAULT_CV_FICHERO)
            .cv_ficheroContentType(DEFAULT_CV_FICHERO_CONTENT_TYPE)
            .apto(DEFAULT_APTO);
        return candidatura;
    }

    @Before
    public void initTest() {
        candidatura = createEntity(em);
    }

    @Test
    @Transactional
    public void createCandidatura() throws Exception {
        int databaseSizeBeforeCreate = candidaturaRepository.findAll().size();

        // Create the Candidatura
        restCandidaturaMockMvc.perform(post("/api/candidaturas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(candidatura)))
            .andExpect(status().isCreated());

        // Validate the Candidatura in the database
        List<Candidatura> candidaturaList = candidaturaRepository.findAll();
        assertThat(candidaturaList).hasSize(databaseSizeBeforeCreate + 1);
        Candidatura testCandidatura = candidaturaList.get(candidaturaList.size() - 1);
        assertThat(testCandidatura.getCv_texto()).isEqualTo(DEFAULT_CV_TEXTO);
        assertThat(testCandidatura.getCv_fichero()).isEqualTo(DEFAULT_CV_FICHERO);
        assertThat(testCandidatura.getCv_ficheroContentType()).isEqualTo(DEFAULT_CV_FICHERO_CONTENT_TYPE);
        assertThat(testCandidatura.isApto()).isEqualTo(DEFAULT_APTO);
    }

    @Test
    @Transactional
    public void createCandidaturaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = candidaturaRepository.findAll().size();

        // Create the Candidatura with an existing ID
        candidatura.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCandidaturaMockMvc.perform(post("/api/candidaturas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(candidatura)))
            .andExpect(status().isBadRequest());

        // Validate the Candidatura in the database
        List<Candidatura> candidaturaList = candidaturaRepository.findAll();
        assertThat(candidaturaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllCandidaturas() throws Exception {
        // Initialize the database
        candidaturaRepository.saveAndFlush(candidatura);

        // Get all the candidaturaList
        restCandidaturaMockMvc.perform(get("/api/candidaturas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(candidatura.getId().intValue())))
            .andExpect(jsonPath("$.[*].cv_texto").value(hasItem(DEFAULT_CV_TEXTO.toString())))
            .andExpect(jsonPath("$.[*].cv_ficheroContentType").value(hasItem(DEFAULT_CV_FICHERO_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].cv_fichero").value(hasItem(Base64Utils.encodeToString(DEFAULT_CV_FICHERO))))
            .andExpect(jsonPath("$.[*].apto").value(hasItem(DEFAULT_APTO.booleanValue())));
    }

    @Test
    @Transactional
    public void getCandidatura() throws Exception {
        // Initialize the database
        candidaturaRepository.saveAndFlush(candidatura);

        // Get the candidatura
        restCandidaturaMockMvc.perform(get("/api/candidaturas/{id}", candidatura.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(candidatura.getId().intValue()))
            .andExpect(jsonPath("$.cv_texto").value(DEFAULT_CV_TEXTO.toString()))
            .andExpect(jsonPath("$.cv_ficheroContentType").value(DEFAULT_CV_FICHERO_CONTENT_TYPE))
            .andExpect(jsonPath("$.cv_fichero").value(Base64Utils.encodeToString(DEFAULT_CV_FICHERO)))
            .andExpect(jsonPath("$.apto").value(DEFAULT_APTO.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingCandidatura() throws Exception {
        // Get the candidatura
        restCandidaturaMockMvc.perform(get("/api/candidaturas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCandidatura() throws Exception {
        // Initialize the database
        candidaturaRepository.saveAndFlush(candidatura);
        int databaseSizeBeforeUpdate = candidaturaRepository.findAll().size();

        // Update the candidatura
        Candidatura updatedCandidatura = candidaturaRepository.findOne(candidatura.getId());
        // Disconnect from session so that the updates on updatedCandidatura are not directly saved in db
        em.detach(updatedCandidatura);
        updatedCandidatura
            .cv_texto(UPDATED_CV_TEXTO)
            .cv_fichero(UPDATED_CV_FICHERO)
            .cv_ficheroContentType(UPDATED_CV_FICHERO_CONTENT_TYPE)
            .apto(UPDATED_APTO);

        restCandidaturaMockMvc.perform(put("/api/candidaturas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCandidatura)))
            .andExpect(status().isOk());

        // Validate the Candidatura in the database
        List<Candidatura> candidaturaList = candidaturaRepository.findAll();
        assertThat(candidaturaList).hasSize(databaseSizeBeforeUpdate);
        Candidatura testCandidatura = candidaturaList.get(candidaturaList.size() - 1);
        assertThat(testCandidatura.getCv_texto()).isEqualTo(UPDATED_CV_TEXTO);
        assertThat(testCandidatura.getCv_fichero()).isEqualTo(UPDATED_CV_FICHERO);
        assertThat(testCandidatura.getCv_ficheroContentType()).isEqualTo(UPDATED_CV_FICHERO_CONTENT_TYPE);
        assertThat(testCandidatura.isApto()).isEqualTo(UPDATED_APTO);
    }

    @Test
    @Transactional
    public void updateNonExistingCandidatura() throws Exception {
        int databaseSizeBeforeUpdate = candidaturaRepository.findAll().size();

        // Create the Candidatura

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCandidaturaMockMvc.perform(put("/api/candidaturas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(candidatura)))
            .andExpect(status().isCreated());

        // Validate the Candidatura in the database
        List<Candidatura> candidaturaList = candidaturaRepository.findAll();
        assertThat(candidaturaList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteCandidatura() throws Exception {
        // Initialize the database
        candidaturaRepository.saveAndFlush(candidatura);
        int databaseSizeBeforeDelete = candidaturaRepository.findAll().size();

        // Get the candidatura
        restCandidaturaMockMvc.perform(delete("/api/candidaturas/{id}", candidatura.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Candidatura> candidaturaList = candidaturaRepository.findAll();
        assertThat(candidaturaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Candidatura.class);
        Candidatura candidatura1 = new Candidatura();
        candidatura1.setId(1L);
        Candidatura candidatura2 = new Candidatura();
        candidatura2.setId(candidatura1.getId());
        assertThat(candidatura1).isEqualTo(candidatura2);
        candidatura2.setId(2L);
        assertThat(candidatura1).isNotEqualTo(candidatura2);
        candidatura1.setId(null);
        assertThat(candidatura1).isNotEqualTo(candidatura2);
    }
}
