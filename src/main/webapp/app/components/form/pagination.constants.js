(function() {
    'use strict';

    angular
        .module('jobnowApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
