(function() {
    'use strict';

    angular
        .module('jobnowApp')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['$state', 'Oferta', 'ParseLinks', 'AlertService', 'paginationConstants', 'pagingParams'];

    function HomeController($state, Oferta, ParseLinks, AlertService, paginationConstants, pagingParams) {

        var vm = this;

        vm.account = null;
        vm.isAuthenticated = null;
        vm.login = LoginService.open;
        vm.register = register;
        $scope.$on('authenticationSuccess', function() {
            getAccount();
        });

        //busqueda
        vm.titulo=null;
    	vm.localidad=null;
    	
        vm.ofertaSearch = ofertaSearch;
        vm.loadPage = loadPage;
        vm.predicate = pagingParams.predicate;
        vm.reverse = pagingParams.ascending;
        vm.transition = transition;
        vm.itemsPerPage = paginationConstants.itemsPerPage;

        loadAll();
        
        function ofertaSearch() {

        	if (vm.titulo !== null || vm.localidad !== null) {
        	
        		let titulo = undefined;
        		if (vm.titulo && vm.titulo !=null) {
        			titulo = vm.titulo;
        		}
        		titulo = titulo || " ";
        		

        		let localidad = undefined;
        		if (vm.localidad && vm.localidad !=null){        			
        			localidad = vm.localidad;
        		}        				
        		localidad = localidad || " ";
        		
        		console.log('titulo: '+vm.titulo);
        		console.log('localidad: '+vm.localidad);

        		console.log('titulo: '+titulo);
        		console.log('localidad: '+localidad);

        		vm.ofertas= Oferta.findByTituloContainingOrLocalidadContaining({titulo: titulo,localidad: localidad});
        	}        	   		
        	
        }
        
        function loadAll () {
            Oferta.query({
                page: pagingParams.page - 1,
                size: vm.itemsPerPage,
                sort: sort()
            }, onSuccess, onError);
            function sort() {
                var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
                if (vm.predicate !== 'id') {
                    result.push('id');
                }
                return result;
            }
            function onSuccess(data, headers) {
                vm.links = ParseLinks.parse(headers('link'));
                vm.totalItems = headers('X-Total-Count');
                vm.queryCount = vm.totalItems;
                vm.ofertas = data;
                vm.page = pagingParams.page;
            }
            function onError(error) {
                AlertService.error(error.data.message);
            }
        }

        function loadPage(page) {
            vm.page = page;
            vm.transition();
        }

        function transition() {
            $state.transitionTo($state.$current, {
                page: vm.page,
                sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc'),
                search: vm.currentSearch
            });
        }
    }    
    	
})();
