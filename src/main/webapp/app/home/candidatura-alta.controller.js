(function() {
    'use strict';

    angular
        .module('jobnowApp')
        .controller('CandidaturaAltaController', CandidaturaAltaController);

    CandidaturaAltaController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'DataUtils', 'entity', 'Candidatura', 'Oferta', 'Usuario'];

    function CandidaturaAltaController ($timeout, $scope, $stateParams, $uibModalInstance, DataUtils, entity, Candidatura, Oferta, Usuario) {

        var vm = this;
//vm.id = 1;
        vm.clear = clear;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;
        vm.save = save;

        vm.saveUsuario = saveUsuario;
        vm.saveCandidaturaAndUsuario = saveCandidaturaAndUsuario;
//COMPROBAR SI VIENE LA OFERTA PARA COLOCARLA EN LA CAJA DEL INPUT
        vm.candidatura = entity;

        if (vm.candidatura.oferta) {
            console.log("id existe: "+vm.candidatura.oferta.id);
        }

        if (vm.candidatura.oferta) {
            //acceder a la oferta a la que vamos a inscribir la candidatura
            vm.oferta = Oferta.get({ id: 1 });
            vm.candidatura.oferta = vm.oferta;
            console.log("vm-oferta")
            console.log(vm.oferta)
        } else {
            vm.ofertas = Oferta.query();
        }
        
        // if (vm.candidatura.usuario && vm.candidatura.usuario.id !== null) {
        //     vm.usuario = Usuario.get({ id: vm.candidatura.usuario.id});
        // } else {
        //     vm.usuarios = Usuario.query();
        // }

        // ALTA NECESARIA DE USUARIO
        vm.telefono = null;
        vm.email = null;
        vm.dni = null;
        vm.altaPendiente = false;
        vm.validando = false;  // protege campos de candidatura
        vm.validado = false;
        vm.validar = validar;

        // if (Usuario){
        //     if (vm.candidatura.usuario !== null){
        //         vm.usuario = vm.candidatura.usuario;
        //     }
        //     else{
        //         vm.usuario = {};
        //     }
        // }
        //validar alta de CANDIDATURA
        function validar(){
            console.log("====================================");
            console.log("   validando...");
            console.log("====================================");

            if (vm.altaPendiente) {
                //YA ESTAN LOS DATOS DE CANDIDATURA
                // se NECESITA ALTA de USUARIO-CANDIDATO
                //  -  validar USUARIO
                //  -  guardar USUARIO Y CANDIDATURA
                console.log("====================================");
                console.log("   altaPendiente...");
                console.log("====================================");
                if (vm.nombre       !== null 
                    &&  vm.apellido !== null
                    &&  vm.dni      !== null) {
                        // guardar USUARIO y recoger el id  
                        console.log("====================================");
                        console.log(" HAY  HAY APELLIDO, HAY DNI");
                        console.log("  NOMBRE   :" + vm.nombre);
                        console.log("  APELLIDO :" + vm.apellido);
                        console.log("  DNI      :" + vm.dni);
                        console.log("====================================");
                    vm.usuario =
                    {
                        nombre:     vm.nombre,
                        apellido:   vm.apellido,
                        dni:        vm.dni,
                        telefono:   vm.telefono,
                        email:      vm.email
                    }
                    vm.saveCandidaturaAndUsuario();
                }
            } else {
                console.log("====================================");
                console.log("   NO NO altaPendiente...");
                console.log("====================================");
                //VALIDAR SI ESTAN LOS DATOS DEL USUARIO 
                if (vm.telefono && vm.email  
                    && vm.telefono  != null
                    && vm.email     != null) {
                    console.log("====================================");
                    console.log("   hay TELEFONO OE EMAIL...");
                    console.log("====================================");
                    //bloquea los datos de candidatura                
                    //abre los datos de usuario adicionales
                    vm.validando = true;
                    vm.alta = true;
                    let usu = comprobarUsuario(vm.telefono, vm.email);
                    console.log("====================================");
                    console.log("   USU?...");
                    console.log(usu);
                    console.log("====================================");
                    if (usu != null) {
                        // asignar USUARIO A OFERTA
                        vm.oferta.usuario = usu;
                        vm.validado = true;
                        // GUARDAR Y VOLVER
                        vm.save();
                    }
                    else{
                        //permitir a la vista introducir(identificación)
                        // USUARIO: nombre, apellidos y dni
                        vm.altaPendiente = true;
                    }
                } else {
                    //permitir a la vista introducir (identificación)
                    // USUARIO: nombre, apellidos y dni
                    vm.altaPendiente = true; 
                }
                
            }
        }

        function comprobarUsuario( telefono, email ){
            console.log("=========================================");
            console.log("llamando a findByEmailAndTelefono");
            console.log("email: "+email);
            console.log("telefono: "+telefono);
            console.log("=========================================");

            //llamando
            return Usuario.findByEmailAndTelefono( email, telefono );
        }

        function saveCandidaturaAndUsuario () {
            vm.isSaving = true;
            if (vm.candidatura.id !== null) {
                Candidatura.update(vm.candidatura, onSaveSuccess, onSaveError);
            } else {
                // Candidatura.save(vm.candidatura, onSaveSuccess, onSaveError);
                Candidatura.save(vm.candidatura, saveUsuario, onSaveError);
            }
        }

        function saveUsuario(){
            Usuario.save(vm.usuario, onSaveUsuarioSuccess, onSaveUsuarioError);
        }

        function onSaveUsuarioSuccess (result) {
            $scope.$emit('jobnowApp:usuarioUpdateParaCandidatura', result);
            console.log("==========================================");
            console.log("****   USUARIO SALVADO    ok ****");
            console.log(result);
            console.log("==========================================");
        }
        function onSaveUsuarioError () {
            console.log("==========================================");
            console.log("***  USUARIO  *****  error SALVANDO ***");
            console.log(result);
            console.log("==========================================");
        }
        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }
        function save () {
            vm.isSaving = true;
            if (vm.candidatura.id !== null) {
                Candidatura.update(vm.candidatura, onSaveSuccess, onSaveError);
            } else {
                Candidatura.save(vm.candidatura, onSaveSuccess, onSaveError);
            }
        }
        function onSaveSuccess (result) {

            $scope.$emit('jobnowApp:candidaturaUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }
        function onSaveError () {
            vm.isSaving = false;
        }
        vm.setCv_fichero = function ($file, candidatura) {
            if ($file) {
                DataUtils.toBase64($file, function(base64Data) {
                    $scope.$apply(function() {
                        candidatura.cv_fichero = base64Data;
                        candidatura.cv_ficheroContentType = $file.type;
                    });
                });
            }
        };
    }
})();
