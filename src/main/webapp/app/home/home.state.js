(function() {
    'use strict';

    angular
        .module('jobnowApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('CandidaturaAltaIdOferta', {
            parent: 'home',
            url: 'altaCandidaturaOferta/{id}',
            data: {
                authorities: []
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                console.log("$stateParams.id: "+$stateParams.id);
                $uibModal.open({
                    templateUrl: 'app/home/candidatura-alta.html',
                    controller: 'CandidaturaAltaController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'md',
                    resolve: {
                        entity: function () {
                            return {
                                cv_texto: null,
                                cv_fichero: null,
                                cv_ficheroContentType: null,
                                apto: null,
                                id: null,
                                oferta : {id: $stateParams.id}
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('home', null, { reload: 'home' });
                }, function() {
                    $state.go('^');
                });
            }]
        }).state('home', {
            parent: 'app',
            url: '/oferta?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'jobnowApp.oferta.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/home/home.html',
                    controller: 'HomeController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('oferta');
                    $translatePartialLoader.addPart('candidatura');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
    }
})();
