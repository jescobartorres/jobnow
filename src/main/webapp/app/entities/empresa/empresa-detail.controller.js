(function() {
    'use strict';
   
    angular
        .module('jobnowApp')
        .controller('EmpresaDetailController', EmpresaDetailController);

    EmpresaDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Empresa', 'Oferta'];

    function EmpresaDetailController($scope, $rootScope, $stateParams, previousState, entity, Empresa, Oferta) {
        var vm = this;
        vm.empresa = entity;        
        vm.previousState = previousState.name;        
        vm.ofertas = Oferta.findByEmpresa({id: vm.empresa.id});
console.log('vm.ofertas');
console.log(vm.ofertas);
        var unsubscribe = $rootScope.$on('jobnowApp:empresaUpdate', function(event, result) {
            vm.empresa = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
