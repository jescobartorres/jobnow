(function() {
    'use strict';

    angular
        .module('jobnowApp')
        .controller('CandidaturaDialogController', CandidaturaDialogController);

    CandidaturaDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'DataUtils', 'entity', 'Candidatura', 'Oferta', 'Usuario'];

    function CandidaturaDialogController ($timeout, $scope, $stateParams, $uibModalInstance, DataUtils, entity, Candidatura, Oferta, Usuario) {
        var vm = this;

        vm.candidatura = entity;
        vm.clear = clear;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;
        vm.save = save;
        vm.ofertas = Oferta.query();
        vm.usuarios = Usuario.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.candidatura.id !== null) {
                Candidatura.update(vm.candidatura, onSaveSuccess, onSaveError);
            } else {
                Candidatura.save(vm.candidatura, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('jobnowApp:candidaturaUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


        vm.setCv_fichero = function ($file, candidatura) {
            if ($file) {
                DataUtils.toBase64($file, function(base64Data) {
                    $scope.$apply(function() {
                        candidatura.cv_fichero = base64Data;
                        candidatura.cv_ficheroContentType = $file.type;
                    });
                });
            }
        };

    }
})();
