(function() {
    'use strict';

    angular
        .module('jobnowApp')
        .controller('CandidaturaDetailController', CandidaturaDetailController);

    CandidaturaDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'DataUtils', 'entity', 'Candidatura', 'Oferta', 'Usuario'];

    function CandidaturaDetailController($scope, $rootScope, $stateParams, previousState, DataUtils, entity, Candidatura, Oferta, Usuario) {
        var vm = this;

        vm.candidatura = entity;
        vm.previousState = previousState.name;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;

        var unsubscribe = $rootScope.$on('jobnowApp:candidaturaUpdate', function(event, result) {
            vm.candidatura = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
