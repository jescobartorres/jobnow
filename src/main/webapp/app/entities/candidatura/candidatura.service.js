(function() {
    'use strict';
    angular
        .module('jobnowApp')
        .factory('Candidatura', Candidatura);

    Candidatura.$inject = ['$resource'];

    function Candidatura ($resource) {
        var resourceUrl =  'api/candidaturas/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' },
            'findByOferta':  { method: 'GET', url: 'api/candidaturas/oferta/:id', isArray: true }
        });
    }
})();
