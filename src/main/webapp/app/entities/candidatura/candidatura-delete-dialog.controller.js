(function() {
    'use strict';

    angular
        .module('jobnowApp')
        .controller('CandidaturaDeleteController',CandidaturaDeleteController);

    CandidaturaDeleteController.$inject = ['$uibModalInstance', 'entity', 'Candidatura'];

    function CandidaturaDeleteController($uibModalInstance, entity, Candidatura) {
        var vm = this;

        vm.candidatura = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Candidatura.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
