(function() {
    'use strict';

    angular
        .module('jobnowApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('candidatura', {
            parent: 'entity',
            url: '/candidatura?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'jobnowApp.candidatura.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/candidatura/candidaturas.html',
                    controller: 'CandidaturaController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('candidatura');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('candidatura-detail', {
            parent: 'candidatura',
            url: '/candidatura/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'jobnowApp.candidatura.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/candidatura/candidatura-detail.html',
                    controller: 'CandidaturaDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('candidatura');
                     return $translate.refresh();
                }],
                entity: ['$stateParams', 'Candidatura', function($stateParams, Candidatura) {
                    return Candidatura.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'candidatura',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('candidatura-detail.edit', {
            parent: 'candidatura-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/candidatura/candidatura-dialog.html',
                    controller: 'CandidaturaDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Candidatura', function(Candidatura) {
                            return Candidatura.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('candidatura.new', {
            parent: 'candidatura',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/candidatura/candidatura-dialog.html',
                    controller: 'CandidaturaDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                cv_texto: null,
                                cv_fichero: null,
                                cv_ficheroContentType: null,
                                apto: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('candidatura', null, { reload: 'candidatura' });
                }, function() {
                    $state.go('candidatura');
                });
            }]
        })
        .state('candidatura.edit', {
            parent: 'candidatura',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/candidatura/candidatura-dialog.html',
                    controller: 'CandidaturaDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Candidatura', function(Candidatura) {
                            return Candidatura.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('candidatura', null, { reload: 'candidatura' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('candidatura.delete', {
            parent: 'candidatura',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/candidatura/candidatura-delete-dialog.html',
                    controller: 'CandidaturaDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Candidatura', function(Candidatura) {
                            return Candidatura.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('candidatura', null, { reload: 'candidatura' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
