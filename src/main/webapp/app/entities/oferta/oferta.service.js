(function() {
    'use strict';
    angular
        .module('jobnowApp')
        .factory('Oferta', Oferta);

    Oferta.$inject = ['$resource', 'DateUtils'];

    function Oferta ($resource, DateUtils) {
        var resourceUrl =  'api/ofertas/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'findByTituloContainingOrLocalidadContaining':{
            	method: 'GET',
            	url:'api/ofertas/titulolocalidad/:titulo/:localidad',
            	isArray:true
            		
            },
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.fecha = DateUtils.convertLocalDateFromServer(data.fecha);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.fecha = DateUtils.convertLocalDateToServer(copy.fecha);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.fecha = DateUtils.convertLocalDateToServer(copy.fecha);
                    return angular.toJson(copy);                
                }
            },
            
            'findByEmpresa': { method: 'GET', url: 'api/ofertas/empresa/:id', isArray: true }
        });
    }
})();
