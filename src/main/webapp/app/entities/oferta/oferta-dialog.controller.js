(function() {
    'use strict';

    angular
        .module('jobnowApp')
        .controller('OfertaDialogController', OfertaDialogController);

    OfertaDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Oferta', 'Empresa'];

    function OfertaDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Oferta, Empresa) {
        var vm = this;

        vm.oferta = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.empresas = Empresa.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.oferta.id !== null) {
                Oferta.update(vm.oferta, onSaveSuccess, onSaveError);
            } else {
                Oferta.save(vm.oferta, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('jobnowApp:ofertaUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.fecha = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
