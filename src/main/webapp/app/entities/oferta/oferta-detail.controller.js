(function() {
    'use strict';

    angular
        .module('jobnowApp')
        .controller('OfertaDetailController', OfertaDetailController);

    OfertaDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Oferta', 'Empresa', 'Candidatura'];

    function OfertaDetailController($scope, $rootScope, $stateParams, previousState, entity, Oferta, Empresa, Candidatura) {
        var vm = this;

        vm.oferta = entity;
  


        vm.previousState = previousState.name;
        vm.candidaturas = Candidatura.findByOferta({id: vm.oferta.id});

        var unsubscribe = $rootScope.$on('jobnowApp:ofertaUpdate', function(event, result) {
            vm.oferta = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
