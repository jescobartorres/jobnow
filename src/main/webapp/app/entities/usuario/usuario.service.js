(function() {
    'use strict';
    angular
        .module('jobnowApp')
        .factory('Usuario', Usuario);

    Usuario.$inject = ['$resource'];

    function Usuario ($resource) {
        var resourceUrl =  'api/usuarios/:id';

        return $resource(resourceUrl, {}, {
            // findByTlfn&email
            'findByEmailAndTelefono': {
                method: 'GET',
                url: 'api/usuarios/emailAndTelefono/:email/:telefono',
                isArray: true
            },
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
