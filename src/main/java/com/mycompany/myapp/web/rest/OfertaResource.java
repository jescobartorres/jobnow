package com.mycompany.myapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.domain.Empresa;
import com.mycompany.myapp.domain.Oferta;
import com.mycompany.myapp.repository.EmpresaRepository;
import com.mycompany.myapp.repository.OfertaRepository;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import com.mycompany.myapp.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Oferta.
 */
@RestController
@RequestMapping("/api")
public class OfertaResource {

    private final Logger log = LoggerFactory.getLogger(OfertaResource.class);

    private static final String ENTITY_NAME = "oferta";

    private final EmpresaRepository empresaRepository;
    
    private final OfertaRepository ofertaRepository;


    public OfertaResource(OfertaRepository ofertaRepository, EmpresaRepository empresaRepository) {
        this.ofertaRepository = ofertaRepository;
        this.empresaRepository = empresaRepository; 
    }

    /**
     * POST  /ofertas : Create a new oferta.
     *
     * @param oferta the oferta to create
     * @return the ResponseEntity with status 201 (Created) and with body the new oferta, or with status 400 (Bad Request) if the oferta has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/ofertas")
    @Timed
    public ResponseEntity<Oferta> createOferta(@RequestBody Oferta oferta) throws URISyntaxException {
        log.debug("REST request to save Oferta : {}", oferta);
        if (oferta.getId() != null) {
            throw new BadRequestAlertException("A new oferta cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Oferta result = ofertaRepository.save(oferta);
        return ResponseEntity.created(new URI("/api/ofertas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /ofertas : Updates an existing oferta.
     *
     * @param oferta the oferta to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated oferta,
     * or with status 400 (Bad Request) if the oferta is not valid,
     * or with status 500 (Internal Server Error) if the oferta couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/ofertas")
    @Timed
    public ResponseEntity<Oferta> updateOferta(@RequestBody Oferta oferta) throws URISyntaxException {
        log.debug("REST request to update Oferta : {}", oferta);
        if (oferta.getId() == null) {
            return createOferta(oferta);
        }
        Oferta result = ofertaRepository.save(oferta);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, oferta.getId().toString()))
            .body(result);
    }

    /**
     * GET  /ofertas : get all the ofertas.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of ofertas in body
     */
    @GetMapping("/ofertas")
    @Timed
    public ResponseEntity<List<Oferta>> getAllOfertas(Pageable pageable) {
        log.debug("REST request to get a page of Ofertas");
        Page<Oferta> page = ofertaRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/ofertas");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
    @GetMapping("/ofertas/titulolocalidad/{titulo}/{localidad}")
    @Timed
    public ResponseEntity<List<Oferta>> getAllOfertasByTituloLocalidad(Pageable pageable, @PathVariable String titulo, @PathVariable String localidad) {
        log.debug("REST request to get a page of Ofertas");       
        log.debug("titulo localidad: {} / {}",titulo,localidad);       
        Page<Oferta> page = ofertaRepository.findByTituloContainingOrLocalidadContaining(pageable, titulo, localidad);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/ofertas");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    
    /**
     *GET  /ofertas/:id : get the "id" oferta.: get all the ofertas por empresa.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of ofertas in body
     */
    @GetMapping("/ofertas/empresa/{id}")
    @Timed   
    public ResponseEntity<List<Oferta>> getAllofertasbyEmpresa(Pageable pageable, @PathVariable Long id) {        
        log.debug("REST request to get Empresa : {}", id);  
        Empresa empresa = empresaRepository.findOne(id);  
        System.out.println("empresa: "+empresa.getNombre());
        Page<Oferta> page = ofertaRepository.findByEmpresa(pageable, empresa); 
        System.out.println("arreglo: "+page.getSize());
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/ofertas"); 
        System.out.println("header: "+headers);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK); 
         
    } 
    
    
    /**
     * GET  /ofertas/:id : get the "id" oferta.
     *
     * @param id the id of the oferta to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the oferta, or with status 404 (Not Found)
     */
    @GetMapping("/ofertas/{id}")
    @Timed
    public ResponseEntity<Oferta> getOferta(@PathVariable Long id) {
        log.debug("REST request to get Oferta : {}", id);
        Oferta oferta = ofertaRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(oferta));
    }

    /**
     * DELETE  /ofertas/:id : delete the "id" oferta.
     *
     * @param id the id of the oferta to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/ofertas/{id}")
    @Timed
    public ResponseEntity<Void> deleteOferta(@PathVariable Long id) {
        log.debug("REST request to delete Oferta : {}", id);
        ofertaRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
