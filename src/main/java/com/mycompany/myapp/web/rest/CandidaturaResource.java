package com.mycompany.myapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.domain.Candidatura;
import com.mycompany.myapp.domain.Empresa;
import com.mycompany.myapp.domain.Oferta;
import com.mycompany.myapp.repository.CandidaturaRepository;
import com.mycompany.myapp.repository.EmpresaRepository;
import com.mycompany.myapp.repository.OfertaRepository;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import com.mycompany.myapp.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

// findByUsuario
import com.mycompany.myapp.domain.Usuario;
import com.mycompany.myapp.repository.UsuarioRepository;

/**
 * REST controller for managing Candidatura.
 */
@RestController
@RequestMapping("/api")
public class CandidaturaResource {

    private final Logger log = LoggerFactory.getLogger(CandidaturaResource.class);

    private static final String ENTITY_NAME = "candidatura";

    private final CandidaturaRepository candidaturaRepository;
    private final UsuarioRepository usuarioRepository;
	private final OfertaRepository ofertaRepository;

    public CandidaturaResource(CandidaturaRepository candidaturaRepository, OfertaRepository ofertaRepository, UsuarioRepository usuarioRepository) {
        this.ofertaRepository = ofertaRepository;
        this.candidaturaRepository = candidaturaRepository;
        this.usuarioRepository = usuarioRepository;
    }
    /**
     * POST  /candidaturas : Create a new candidatura.
     *
     * @param candidatura the candidatura to create
     * @return the ResponseEntity with status 201 (Created) and with body the new candidatura, or with status 400 (Bad Request) if the candidatura has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/candidaturas")
    @Timed
    public ResponseEntity<Candidatura> createCandidatura(@RequestBody Candidatura candidatura) throws URISyntaxException {
        log.debug("REST request to save Candidatura : {}", candidatura);
        if (candidatura.getId() != null) {
            throw new BadRequestAlertException("A new candidatura cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Candidatura result = candidaturaRepository.save(candidatura);
        return ResponseEntity.created(new URI("/api/candidaturas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }
    /**
     * PUT  /candidaturas : Updates an existing candidatura.
     *
     * @param candidatura the candidatura to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated candidatura,
     * or with status 400 (Bad Request) if the candidatura is not valid,
     * or with status 500 (Internal Server Error) if the candidatura couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/candidaturas")
    @Timed
    public ResponseEntity<Candidatura> updateCandidatura(@RequestBody Candidatura candidatura) throws URISyntaxException {
        log.debug("REST request to update Candidatura : {}", candidatura);
        if (candidatura.getId() == null) {
            return createCandidatura(candidatura);
        }
        Candidatura result = candidaturaRepository.save(candidatura);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, candidatura.getId().toString()))
            .body(result);
    }
    /**
     * GET  /candidaturas : get all the candidaturas.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of candidaturas in body
     */
    @GetMapping("/candidaturas")
    @Timed
    public ResponseEntity<List<Candidatura>> getAllCandidaturas(Pageable pageable) {
        log.debug("REST request to get a page of Candidaturas");
        Page<Candidatura> page = candidaturaRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/candidaturas");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
    /**
     * GET  /candidaturas/:id : get the "id" candidatura.
     *
     * @param id the id of the candidatura to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the candidatura, or with status 404 (Not Found)
     */
    @GetMapping("/candidaturas/{id}")
    @Timed
    public ResponseEntity<Candidatura> getCandidatura(@PathVariable Long id) {
        log.debug("REST request to get Candidatura : {}", id);
        Candidatura candidatura = candidaturaRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(candidatura));
    }

    
    /**
     *GET  /candidaturas/:id : get the "id" candidaturas.: get all the candidaturas por oferta.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of candidaturas in body
     */
    @GetMapping("/candidaturas/oferta/{id}")
    @Timed   
    public ResponseEntity<List<Candidatura>> getAllofertasbyEmpresa(Pageable pageable, @PathVariable Long id) {        
        log.debug("REST request to get Oferta : {}", id);  
        Oferta oferta = ofertaRepository.findOne(id);  
        System.out.println("oferta: "+oferta.getTitulo());
        Page<Candidatura> page = candidaturaRepository.findByOferta(pageable, oferta); 
        System.out.println("arreglo: "+page.getSize());
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/candidaturas"); 
        System.out.println("header: "+headers);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK); 
         
    } 
    
   

    /**
     * DELETE  /candidaturas/:id : delete the "id" candidatura.
     *
     * @param id the id of the candidatura to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/candidaturas/{id}")
    @Timed
    public ResponseEntity<Void> deleteCandidatura(@PathVariable Long id) {
        log.debug("REST request to delete Candidatura : {}", id);
        candidaturaRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
    // findByUser
    @GetMapping("/candidaturas/usuario/{id}")
    @Timed    
    public ResponseEntity<List<Candidatura>> getCandidaturaByUsusario(@PathVariable Long id){
        log.debug("REST API USUARIO REQUEST BY OFERTA ID: {}", id);
        Usuario u =usuarioRepository.findOne(id);
        List<Candidatura> lista = candidaturaRepository.findByUsuario(u);
        return new ResponseEntity<>(lista, HttpStatus.OK);
    }
}