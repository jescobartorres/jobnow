package com.mycompany.myapp.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Oferta.
 */
@Entity
@Table(name = "oferta")
public class Oferta implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "titulo")
    private String titulo;

    @Column(name = "detalle")
    private String detalle;

    @Column(name = "localidad")
    private String localidad;

    @Column(name = "fecha")
    private LocalDate fecha;

    @Column(name = "tipo_contrato")
    private String tipo_contrato;

    @Column(name = "jornada")
    private String jornada;

    @Column(name = "salario")
    private Integer salario;

    @ManyToOne
    private Empresa empresa;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public Oferta titulo(String titulo) {
        this.titulo = titulo;
        return this;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDetalle() {
        return detalle;
    }

    public Oferta detalle(String detalle) {
        this.detalle = detalle;
        return this;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public String getLocalidad() {
        return localidad;
    }

    public Oferta localidad(String localidad) {
        this.localidad = localidad;
        return this;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public Oferta fecha(LocalDate fecha) {
        this.fecha = fecha;
        return this;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public String getTipo_contrato() {
        return tipo_contrato;
    }

    public Oferta tipo_contrato(String tipo_contrato) {
        this.tipo_contrato = tipo_contrato;
        return this;
    }

    public void setTipo_contrato(String tipo_contrato) {
        this.tipo_contrato = tipo_contrato;
    }

    public String getJornada() {
        return jornada;
    }

    public Oferta jornada(String jornada) {
        this.jornada = jornada;
        return this;
    }

    public void setJornada(String jornada) {
        this.jornada = jornada;
    }

    public Integer getSalario() {
        return salario;
    }

    public Oferta salario(Integer salario) {
        this.salario = salario;
        return this;
    }

    public void setSalario(Integer salario) {
        this.salario = salario;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public Oferta empresa(Empresa empresa) {
        this.empresa = empresa;
        return this;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Oferta oferta = (Oferta) o;
        if (oferta.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), oferta.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Oferta{" +
            "id=" + getId() +
            ", titulo='" + getTitulo() + "'" +
            ", detalle='" + getDetalle() + "'" +
            ", localidad='" + getLocalidad() + "'" +
            ", fecha='" + getFecha() + "'" +
            ", tipo_contrato='" + getTipo_contrato() + "'" +
            ", jornada='" + getJornada() + "'" +
            ", salario=" + getSalario() +
            "}";
    }
}
