package com.mycompany.myapp.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Candidatura.
 */
@Entity
@Table(name = "candidatura")
public class Candidatura implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "cv_texto")
    private String cv_texto;

    @Lob
    @Column(name = "cv_fichero")
    private byte[] cv_fichero;

    @Column(name = "cv_fichero_content_type")
    private String cv_ficheroContentType;

    @Column(name = "apto")
    private Boolean apto;

    @ManyToOne
    private Oferta oferta;

    @ManyToOne
    private Usuario usuario;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCv_texto() {
        return cv_texto;
    }

    public Candidatura cv_texto(String cv_texto) {
        this.cv_texto = cv_texto;
        return this;
    }

    public void setCv_texto(String cv_texto) {
        this.cv_texto = cv_texto;
    }

    public byte[] getCv_fichero() {
        return cv_fichero;
    }

    public Candidatura cv_fichero(byte[] cv_fichero) {
        this.cv_fichero = cv_fichero;
        return this;
    }

    public void setCv_fichero(byte[] cv_fichero) {
        this.cv_fichero = cv_fichero;
    }

    public String getCv_ficheroContentType() {
        return cv_ficheroContentType;
    }

    public Candidatura cv_ficheroContentType(String cv_ficheroContentType) {
        this.cv_ficheroContentType = cv_ficheroContentType;
        return this;
    }

    public void setCv_ficheroContentType(String cv_ficheroContentType) {
        this.cv_ficheroContentType = cv_ficheroContentType;
    }

    public Boolean isApto() {
        return apto;
    }

    public Candidatura apto(Boolean apto) {
        this.apto = apto;
        return this;
    }

    public void setApto(Boolean apto) {
        this.apto = apto;
    }

    public Oferta getOferta() {
        return oferta;
    }

    public Candidatura oferta(Oferta oferta) {
        this.oferta = oferta;
        return this;
    }

    public void setOferta(Oferta oferta) {
        this.oferta = oferta;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public Candidatura usuario(Usuario usuario) {
        this.usuario = usuario;
        return this;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Candidatura candidatura = (Candidatura) o;
        if (candidatura.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), candidatura.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Candidatura{" +
            "id=" + getId() +
            ", cv_texto='" + getCv_texto() + "'" +
            ", cv_fichero='" + getCv_fichero() + "'" +
            ", cv_ficheroContentType='" + getCv_ficheroContentType() + "'" +
            ", apto='" + isApto() + "'" +
            "}";
    }
}
