package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Usuario;
import org.springframework.stereotype.Repository;

import java.util.List;
import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Usuario entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {
    //@query("select a  from Usuario a where a.telefono = 0? and a.email = 1?")
//     @query("select u from usuario u where u.telefono = ?1 and u.email = ?2")
// public Usuario findByEmailTelefono( String email, String telefono );

public List<Usuario> findByEmailAndTelefono(String telefono, String email );

}
