package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Empresa;
import com.mycompany.myapp.domain.Oferta;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Oferta entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OfertaRepository extends JpaRepository<Oferta, Long> {
	
	 //@Query(Value ="SELECT * FROM OFERTA WHERE TITULO LIKE "%java%" AND LOCALIDAD = ?2" nativeQuery= true);
	 // @Query("Select o from Oferta o where o.titulo = ?1 and o.localidad = ?2");
	Page<Oferta>findByTituloContainingOrLocalidadContaining(Pageable pageable, String titulo, String localidad);
	


	      Page<Oferta> findByEmpresa(Pageable pageable, Empresa empresa); 
	      
}
