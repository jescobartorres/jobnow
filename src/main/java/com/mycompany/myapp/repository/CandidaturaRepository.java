package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Candidatura;
import com.mycompany.myapp.domain.Empresa;
import com.mycompany.myapp.domain.Oferta;

import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;

import java.util.List;
import com.mycompany.myapp.domain.Usuario;

/**
 * Spring Data JPA repository for the Candidatura entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CandidaturaRepository extends JpaRepository<Candidatura, Long> {
	
    Page<Candidatura> findByOferta(Pageable pageable, Oferta oferta); 

    public List<Candidatura> findByUsuario(Usuario u);
}
